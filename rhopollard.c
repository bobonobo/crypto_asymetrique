#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gmp.h>

void f(mpz_t r,mpz_t n){
	mpz_mul(r,n,n);
	mpz_add_ui(r,r,1);
}

void rhoPollard(mpz_t n){

	mpz_t x,y,d,r1,r2,r3;
	mpz_inits(x,y,d,r1,r2,r3,NULL);
	mpz_set_ui(r1,0);
	mpz_set_ui(r2,0);
	mpz_set_ui(r3,0);
	mpz_set_ui(x,2);
	mpz_set_ui(y,2);
	mpz_set_ui(d,1);
	
	while(mpz_cmpabs_ui(d,1)){
		f(r1,x);
		mpz_mod(x,r1,n);
		f(r2,y);
		f(r3,r2);
		mpz_mod(y,r3,n);
		mpz_sub(r1,x,y);
		mpz_gcd(d,r1,n);
	}
	gmp_printf("%Zd se divise par %Zd \n",n,d);
	//mpz_clears(x,y,d,r1,r2,r3,NULL);
}


int main(int arc, char *argv[]){

	mpz_t n;
	mpz_init(n);
	mpz_set_str(n,"52590354472497239257283147",10);
	rhoPollard(n);
	mpz_set_str(n,"21",10);
	rhoPollard(n);
	mpz_clear(n);

}