#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

long long int lead_exp(long long int m)
{
	if(m==0)
	{
		return 0;
	}

	long long int tmp=m;
	long long int i =0;
	while(tmp!=0)
	{
		tmp=tmp>>1;
		i++;
	}
	i--;
	return i;
}

long long int binaire(long long int m)
{
	if(m<0){
		return -1;
	}
	long long int tmp=m;
	long long int exp = lead_exp(m);
	//exp--;
	long long int r = 0;
	while(exp!=-1)
	{
		if (( (tmp>>(exp))&1)==1)
		{
			r=r+1;
		}
		exp--;
		r=(r<<3)+(r<<1);
		
	}

	return r/10;
}
int Taille(long long unsigned int x)
{
	if(x==0)
	{
		return 0;
	}
	int t=0;	
	while(x!=0)
	{
		x>>=1;
		t++;
	}
	return t;
}

long long unsigned int Mult(long long unsigned int a, long long unsigned int b){
	long long unsigned int p=0;
	for(int i=0;i<Taille(b);i++){
		if( ((b>>i)&1) ==1){
			p=p+(a<<i);	
		}		
	}
	return p;
}
//*
long long unsigned int DivPositif(long long unsigned int *r, long long unsigned int a, long long unsigned int b){
	long long unsigned int pt;
	long long unsigned int gt;
	long long unsigned int q=1;
	if(b==1){
		*r=0;
		return a;
	}
	else if((b==0)||(b>a)){
		*r=a;
		return 0;
	}
	else if((a>b)){
		pt=b;
		gt=a;
	}
	else if((b==a)){
		*r=0;
		return 1;
	}
	while(pt<=gt){
		pt=pt<<1;
		q=q<<1;
	}
	pt=pt>>1;
	q=q>>1;
	while(pt<=gt){
		pt+=b;
		q++;
	}
	pt-=b;
	q--;
	*r=gt-pt;
	return q;
}
//*/

long long unsigned int pgcd(long long unsigned int x, long long unsigned int y){
	long long unsigned int a=x;
	long long unsigned int b=y;
	long long unsigned int d=0;
	long long unsigned int abs;
	long long unsigned int min;
	while(a!=b){
		if( (a&1)==0 && (b&1)==0 ){
			a=(a>>1);
			b=(b>>1);
			d++;
		}
		else if((a&1)==0 && (b&1)==1 ){
			a=(a>>1);
		}
		else if((a&1)==1 && (b&1)==0 ){
			b=(b>>1);
		}
		else{
			if(a>b)
			{
				abs = a-b;
				min = b;
			}
			else
			{
				abs = b-a;
				min =a;
			}
			a=abs;
			b=min;	
		}

	}

	return (1<<d)*a;

}
long long unsigned int Abs(long long int x){
 long long int a;
 a = (x>=0)? x:-x;
 return (long long unsigned int) a; 	
}

long long unsigned int mulmod(long long unsigned int a, long long unsigned int n, long long unsigned int m ){
	long long unsigned int r1;
	long long unsigned int r2;
	long long unsigned int r3;
	long long unsigned int r4;
	DivPositif(&r1,a,m);
	DivPositif(&r2,n,m);
	//r3=Mult(r1,r2);
	r3=r2*r1;
	DivPositif(&r4,r3,m);
	return r4;

}

long long unsigned int sqrmod1(long long unsigned int a, long long unsigned int m){
	long long unsigned int r1;
	long long unsigned int r2;
	DivPositif(&r1,a,m);
	//r2=Mult(r1,r1);
	r2=r1*r1;
	DivPositif(&r2,r2,m);
	return r2;	
}

long long unsigned int sqrmod2(long long unsigned int a, long long unsigned int m){
	return mulmod(a,a,m);		
}
long long unsigned int expmod1(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	if((n&1)==0 && n>0){
		return sqrmod1(expmod1(a,(n>>1),m),m);
	}
	else if((n&1)==1 && n>0){
		return mulmod(a,expmod1(a,n-1,m),m);
	}
	if (n==0){
		return 1;
	}
	else return -1;
}
long long unsigned int expmod2(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	int t= Taille(n);
	//printf("n: %lld, taille de n:%d\n",n,t);
	long long unsigned int p=1;
	long long unsigned int asq =a;//sqrmod1(a,m);
	if (n==0){
		return 1;
	}
	if((n&1)==1){	
		p=a;
	} 
	for (int i=1;i<t;i++){
		asq=sqrmod1(asq,m);
		if(((n>>i)&1)==1){
			p=mulmod(p,asq,m);
		}		
	}
	return p;
}
long long unsigned int expmod3(long long unsigned int a,long long unsigned int n,long long unsigned int m){
	int t= Taille(n);
	long long unsigned int p=1;
	long long unsigned int asq =a;//sqrmod1(a,m);
	if (n==0){
		return 1;
	}
	if((n&1)==1){	
		p=a;
	} 
	int i=1;
	while (i<t){
		asq=sqrmod1(asq,m);
		if(((n>>i)&1)==1){
			p=mulmod(p,asq,m);
		}		
		i++;
	}
	return p;
}
long long unsigned int BezoutBinaire(long long int*pu,long long int*pv,long long unsigned int a,long long unsigned int b){
	long long unsigned int r0;
	long long unsigned int r1;
	if(a<b){
		r0 = a;
		r1 = b;
	}
	else{
		r0 = b;
		r1 = a;	
	}
	long long int u0 = 1;
	long long int u1 = 0;
	long long int v0 = 0;
	long long int v1 = 1;
	long long unsigned int q;
	long long unsigned int rt0;
	long long unsigned int rt1;
	long long int ut0;
	long long int ut1;
	long long int vt0;
	long long int vt1;
	long long unsigned int r;
	while((r1!=0) && (r0!=0)){
		q=DivPositif(&r,r0,r1);
		rt0=r0;
		rt1=r1;
		r0=rt1;
		r1=rt0-Mult(q,rt1);
		ut0=u0;
		ut1=u1;
		u0=ut1;
		u1=ut0-Mult(q,ut1);
		vt0=v0;
		vt1=v1;
		v0=vt1;
		v1=vt0-Mult(q,vt1);
	}
	if(r0==0){
		*pu=ut0;
		*pv=vt0;
		return rt0; 
	}
	else{
		*pu =u0;
		*pv =v0;
		return r0;
	}
	
	

}
long long unsigned int racine(long long unsigned int n){
	long long unsigned int x=n;
	long long unsigned int y=1;
	long long unsigned int r;
	while(x>y){
		x=(x+y)>>1;
		y=DivPositif(&r,n,x);
	}
	return x;
}

long long unsigned int phi(long long unsigned int n){
	long long unsigned int nt=n;
	long long unsigned int p0;
	long long unsigned int p1;
	long long unsigned int phit=1;
	long long int i = 2;
	while(i<=n){
		long long unsigned int r=0;
		long long unsigned int q;
		p0=0;
		p1=1;
		while(r==0){				
			q= DivPositif(&r,nt,i);
			if(r==0){
				//pt=p0;
				p0=p1;
				//p1=Mult(p1,i);
				p1=p1*i;
				nt=q;
			}
		}
		
		if(p1!=p0){
			//phit=Mult(phit,p1-p0);
			phit=phit*(p1-p0);
		}
		i++;
	}
	return phit;
}

int last_one(long long unsigned int x)
{
	if(x==0)
	{
		return -1;
	}
	int l=0;
	long long unsigned int m=x;
	while((m&1)==0)
	{
		l++;
		m=(m>>1);
	}
	return l;
}
//*
int jacobi(long long int p, long long int q)
{
	int j=1;
	int k = last_one(p);
	if((p==0)||(p==1))
	{
		return 1;
	}
	while(p<0)
	{
		p=q+p;
	}
	long long unsigned int r;
	long long unsigned int rq;
	long long unsigned int rp4;
	long long unsigned int rq4;
	long long int temp;
	if(p>=q)
	{
		DivPositif(&r,p,q);	
		p=r;
	}
	else
	{
		DivPositif(&r,q,p);
	}
	if(r==0)
	{
		return 0;
	}
	while(q!=1)
	{
		if(p>q)
		{
			DivPositif(&r,p,q);
			if(r==0)
			{
				return 0;
			}	
			p=r;		
		}
		k = last_one(p);
		rq=q&7;
		if(((rq==3)||(rq==5))&&((k&1)==1)){
			j=-j;
		}

		p=(p>>k);
		rp4=p&3;	
		rq4=q&3;
		if((rp4==3)&&(rq4==3))
		{
			j=-j;
		}
		temp=p;
		p=q;
		q=temp;
	}
	return j;
}

int fermat(long long unsigned int n, int p){
	int k=0;
	srand(time(NULL));
	while(k<p){
			long long unsigned int b = rand()%(n-2)+1;
			if((n&1)==0){
				printf("%lld composé (Fermat)\n",n);
				return 0;
			}
			if(pgcd(b,n)==1){
				if(expmod3(b,n-1,n)!=1){
					printf("%lld composé (Fermat)\n",n);
					return 0;
				}
				else{
					k++;
				}
			}
			else{
				printf("%lld composé (Fermat)\n",n);
				return 0;
			}
	}
	float d =(float)(1<<p);
	//printf("d:%f \n",d);
	float prob =1-1/d;
	printf("%lld probablement premier, p>= %f (Fermat)\n",n,prob);

	return 1;
}

int solovay(long long unsigned int n, int p){
	int k=0;
	srand(time(NULL));
	while(k<p){

			long long unsigned int b = rand()%(n-3)+2;
			if((n&1)==0){
				printf("%lld composé (Solovay Strassen)\n",n);
				return 0;
			}
			if(pgcd(b,n)==1){
				long long unsigned int e = expmod3(b,((n-1)>>1),n);
				long long int j =jacobi((long long int) b,(long long int)n);
				j=(j<0)?j+n:j;
				if((long long int)e!=j){
					printf("%lld composé (Solovay Strassen)\n",n);
					return 0;
				}
				else{
					k++;
				}
			}
			else{
				printf("%lld composé (Solovay Strassen)\n",n);
				return 0;
			}
	}
	float d =(float)(1<<p);
	float prob =1-1/d;
	printf("%lld probablement premier, p>= %f (Solovay Strassen)\n",n,prob);
	return 1;
}
int isWitness(long long unsigned int a,long long unsigned int d,long long unsigned int n, int s){
	long long unsigned int e=expmod3(a,d,n);
	if(e==1||e==n-1){
		return 0;
	}
	else{
		for(int i=0;i<s-1;i++){
			e=sqrmod1(e,n);
			if(e==n-1){
				return 0;			
			}
		}
	}
	return 1;
}

int millerRabin(long long unsigned int n, int p){
	if((n&1)==0){
		printf("%lld composé (Miller Rabin)\n",n);
		return 0;
	}
	int s=0;
	long long unsigned int d=1;
	long long unsigned int temp = n-1;
	while((temp&1)==0){
		temp>>=1;
		s++;
	}
	d=((n-1)>>s);
	int k=0;
	srand(time(NULL));
	while(k<p){
			long long unsigned int a = rand()%(n-2)+2;
			if(isWitness(a,d,n,s)==1){
				printf("%lld composé (Miller Rabin)\n",n);
				return 0;
			}
			k++;
	}
	double pui =(double)pow(0.25,(double)p);
	//printf("d:%f \n",d);
	double prob =1-pui;
	printf("%lld probablement premier, p>= %f (Miller Rabin)\n",n,prob);
	return 1;
}



//*/