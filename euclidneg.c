#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

long long int lead_exp(long long int m)
{
	if(m==0)
	{
		return 0;
	}

	long long int tmp=m;
	long long int i =0;
	while(tmp!=0)
	{
		tmp=tmp>>1;
		i++;
	}
	i--;
	return i;
}
void division(int a, int b,int *q, int *r)
{
	int i= 0;
	int t= b;
	while(t < a)
	{
		t=(t<<1);
		i++;
	}
	*q=1<<i;
	*r=*a-*q;
	while(*r> b)
	{
		*r=*r-b;
		*q=*q+1;
	}
}

unsigned int EuclideBinaire(unsigned a, unsigned b)
{
	if(a==b)
	{
		return a;
	}
	if((a&1)==0 && (b&1)==0 )
	{
		return (EuclideBinaire(a>>1,b>>1)<<1);
	}
	else if((a&1)==0 && (b&1)==1 )
	{
		return EuclideBinaire(a>>1,b);
	}

	else if((a&1)==1 && (b&1)==0 )
	{
		return EuclideBinaire(a,b>>1);
	}

	else
	{//(a&1)==1 && (b&1)==1 
		unsigned int abs =(a-b);
		unsigned int min = b;
 
		if(b>a)
		{
			abs=b-a;
			min = a;
		}


		return EuclideBinaire(abs,min);
	}

}