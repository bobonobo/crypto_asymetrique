#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions2.h"

int last_one(long long unsigned int x){
	if(x==0){
		return -1;
	}
	int l=0;
	long long unsigned int m=x;
	while((m&1)==0){
		l++;
		m=(m>>1);
	}
	return l;
}
int jacobi(long long int p, long long int q){
	int j=1;
	int k = last_one(p);
	if((p==0)||(p==1)){
		return 1;
	}
	if(p<0){
		p=q+p;
	}
	long long unsigned int r;
	long long unsigned int rq;
	long long unsigned int rp4;
	long long unsigned int rq4;
	long long int temp;
	if(p>=q){
		DivPositif(&r,p,q);	
		p=r;
	}
	else{
		DivPositif(&r,q,p);
	}
	if(r==0){
		return 0;
	}
	while(q!=1){
		if(p>q){
			DivPositif(&r,p,q);
			if(r==0){
				return 0;
			}	
			p=r;		
		}
		k = last_one(p);
		rq=q-((q>>3)<<3);
		if((rq==3)||(rq==5)){
			if((k&1)==1){
				j=-j;
			}	
		}		
		p=(p>>k);
		rp4=p-((p>>2)<<2);	
		rq4=q-((q>>2)<<2);
		if((rp4==3)&&(rq4==3)){
			j=-j;
		}
		temp=p;
		p=q;
		q=temp;
	}
	return j;
}
int main(int argc, char **argv){
	
	printf("jacobi(14,51): %d\n",jacobi(14,51));
	printf("jacobi(62,91): %d\n",jacobi(62,91));
	printf("jacobi(51,121): %d\n",jacobi(51,121));
	printf("jacobi(2,13): %d\n",jacobi(2,13));
	printf("jacobi(200,229): %d\n",jacobi(200,229));
	printf("jacobi(153,191): %d\n",jacobi(153,191));
	printf("jacobi(35,101): %d\n",jacobi(35,101));
	printf("jacobi(29,115): %d\n",jacobi(29,115));
	printf("jacobi(88,101): %d\n",jacobi(88,101));
	printf("jacobi(32,121): %d\n",jacobi(32,121));
	printf("jacobi(32,125): %d\n",jacobi(32,125));
	printf("jacobi(62,91): %d\n",jacobi(62,91));
	printf("jacobi(219,383): %d\n",jacobi(219,383));
	printf("jacobi(-1,5): %d\n",jacobi(-1,5));
	printf("jacobi(541,2011): %d\n",jacobi(541,2011));
	printf("jacobi(158,235): %d\n",jacobi(158,235));
	for(long long int i=1;i<21;i++){
		if((jacobi(i,3)==1)&&(jacobi(i,7)==1)){
			printf("%lld est un résidu quadratique\n",i);
		}
		else{
			printf("%lld n'est pas un résidu quadratique\n",i);	
		}
	}

	return 0;

}