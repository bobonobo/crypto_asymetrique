#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions2.h"

int last_one(long long unsigned int x)
{
	if(x==0)
	{
		return -1;
	}
	int l=0;
	long long unsigned int m=x;
	while((m&1)==0)
	{
		l++;
		m=(m>>1);
	}
	return l;
}
int jacobi(long long int p, long long int q)
{
	int j=1;
	int k = last_one(p);
	if((p==0)||(p==1))
	{
		return 1;
	}
	if(p<0)
	{
		p=q+p;
	}
	long long unsigned int r;
	long long unsigned int rq;
	long long unsigned int rp4;
	long long unsigned int rq4;
	long long int temp;
	if(p>=q)
	{
		DivPositif(&r,p,q);	
		p=r;
	}
	else
	{
		DivPositif(&r,q,p);
	}
	if(r==0)
	{
		return 0;
	}
	while(q!=1)
	{
		if(p>q)
		{
			DivPositif(&r,p,q);
			if(r==0)
			{
				return 0;
			}	
			p=r;		
		}
		k = last_one(p);
		rq=q-((q>>3)<<3);
		if((rq==3)||(rq==5))
		{
			if((k&1)==1)
			{
				j=-j;
			}	
		}		
		p=(p>>k);
		rp4=p-((p>>2)<<2);	
		rq4=q-((q>>2)<<2);
		if((rp4==3)&&(rq4==3))
		{
			j=-j;
		}
		temp=p;
		p=q;
		q=temp;
	}
	return j;
}
void shanks(long long int a, long long int p,long long int * r1, long long int * r2)
{
	long long int n = 2;
	while(jacobi(n,p)!=-1)
	{
		n++;
	}
	long long int alpha = 0;
	long long int s = p-1;
	while ((s&1)==0)
	{
		alpha++;
		s>>=1;
	}
	long long int r = expmod1(a,(s+1)>>1,p);
	long long int b = expmod1(n,s,p);
	long long int bs =sqrmod1(b,p);
	long long int y = expmod1(a,s,p);
	long long int tmp;
	long long int tmpb;
	long long int j=0;
	printf("alpha=%lld\n",alpha);
	printf("s=%lld\n",s);
	
	
	//long long tmpb=1;
	for (long long int i = alpha-2;i>=0;i--)
	{	
		tmpb=expmod1(bs,j,p);
		tmp=mulmod(tmpb,y,p);
		tmp=expmod1(tmp,1<<i,p);
		if(tmp==p-1)
		{			
			j=j+(1<<(alpha-2-i));
		}
	
	}
	long long int z=0;
	
	printf("j=%lld\n",j);
	z=expmod1(b,j,p);
	*r1=mulmod(z,r,p);
	*r2=p-*r1;
	//*r2=mulmod(*r1,expmod1(b,1<<(alpha-1),p),p);
 
}
int main(int argc, char **argv){
	int t ;
//	int t2;
	for(long long unsigned int i =0; i <= 10;i++)
	{
		t=Taille(i);
		//t2= tail(i);
		printf("i:%lld\n",i);
		printf("binaire(i):%lld\n",(long long int)binaire(i));
		printf("Taille(%lld):%d\n",i,t);
		//printf("tail(%lld):%d\n",i,t2);
	}
	
	long long int r1;
	long long int r2;
	shanks(302,2081,&r1,&r2);
	printf("r1:%lld\n",r1);
	printf("r2:%lld\n",r2);
	
	return 0;

}