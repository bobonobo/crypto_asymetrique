#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <unistd.h> //pour sleep


long long int lead_exp(long long int m)
{
	if(m==0)
	{

		return 0;
	}

	long long int tmp=m;
	long long int i =0;
	while(tmp!=0)
	{
		tmp=tmp>>1;
		i++;
	}
	i--;
	return i;
}


long long int binaire(long long int m)
{
	if(m<0){
		return -1;
	}
	long long int tmp=m;
	long long int exp = lead_exp(m);
	//exp--;
	long long int r = 0;
	while(exp!=-1)
	{
		if (( (tmp>>(exp))&1)==1)
		{
			r=r+1;
		}
		exp--;
		r=(r<<3)+(r<<1);
		
	}

	return r/10;
}

long long int karat(long long int m, long long int n, long long int exp)
{
	if(n<0){
		n=-n;		
	}

	if(m<0){
		m=-m;		
	}



	if((n==0)||(m==0)){
		/*
		printf("r = 0");
		printf("\n");
		//*/
		return 0;
	}
	
	long long int exp_moit = (exp>>1)+1;
	if(exp==2){
		exp_moit=1;
	}
	
	if(exp==0){
		if(n==1 && m==1)
		{
			/*
			printf("r = 1");
			printf("\n");
			//*/
			return 1;
		}
		/*
		printf("r = 0");
		printf("\n");
		//*/
		return 0 ;
	}
	
	if(exp==1)
	{
		
		exp_moit=1;
	
		long long int gm = m>>exp_moit;
		long long int gn = n>>exp_moit;
		long long int drm = m-(gm<<exp_moit);
		long long int drn = n-(gn<<exp_moit);	
		/*
		printf("m s'écrit %lld",m);
		printf("\n");
		printf("n %lld",n);
		printf("\n");


		printf("exp %lld ",exp);
		printf("\n");

		printf("exp_moit %lld ",exp_moit);
		printf("\n");

		printf("En binaire gm = %lld s'écrit %lld",gm,binaire(gm));
		printf("\n");

		printf("En binaire gn = %lld s'écrit %lld",gn,binaire(gn));
		printf("\n");


		printf("En binaire drm = %lld s'écrit %lld",drm,binaire(drm));
		printf("\n");


		printf("En binaire drn = %lld s'écrit %lld",drn,binaire(drn));
		printf("\n");
		//*/




		long long int ac;
		if(gm==1 && gn==1)
		{
			ac= 1;
		}
		else
		{
			ac=0;
		} 
		long long int bd;
		if(drm==1 && drn==1)
		{
			bd= 1;
		}
		else
		{
			bd=0;
		} 
		long long int abcd=0; 
		
		if(((gm-drm)==1) && ((gn-drn)==1))
		{
			abcd= 1;
		}
		else if(((gm-drm)==1) && ((gn-drn)==-1))
		{
			abcd= -1;
		}
		else if(((gm-drm)==-1) && ((gn-drn)==1))
		{
			abcd= -1;
		}
			//sleep(1);	
		
		long long int r = ( ac<<2 )+(( ac + bd - abcd )<<1 )+ bd;
/*
		printf("r:%lld",r);
		printf("\n");
//*/
		return r ;

	}
	
	
	long long int gm = m>>exp_moit;
	long long int gn = n>>exp_moit;
	long long int drm = m-(gm<<exp_moit);
	long long int drn = n-(gn<<exp_moit);
	/*
	printf("m s'écrit %lld",m);
	printf("\n");
	printf("n %lld",n);
	printf("\n");


	printf("exp %lld ",exp);
	printf("\n");

	printf("exp_moit %lld ",exp_moit);
	printf("\n");

	printf("En binaire gm = %lld s'écrit %lld",gm,binaire(gm));
	printf("\n");

	printf("En binaire gn = %lld s'écrit %lld",gn,binaire(gn));
	printf("\n");


	printf("En binaire drm = %lld s'écrit %lld",drm,binaire(drm));
	printf("\n");


	printf("En binaire drn = %lld s'écrit %lld",drn,binaire(drn));
	printf("\n");
	//*/
	
	//sleep(1);
	/*	
	printf("calcul de ac:");
	printf("\n");
	//*/
	long long int ac = karat(gm,gn,exp-exp_moit);
	/*
	printf("calcul de bd:");
	printf("\n");
	//*/
	long long int bd = karat(drm,drn,exp_moit-1);
	/*
	printf("calcul de ab-cd = (gm-drm)*(gn-drn)");
	printf("\n");

	printf("(gm-drm):%lld",gm-drm);
	printf("\n");

	printf("(gn-drn):%lld",gn-drn);
	printf("\n");
	//*/
	long long int abcd;
	if((gm-drm<0)&&(gn-drn<0))
	{
		long long int tmp1 = lead_exp(drm-gm);
		long long int tmp2 = lead_exp(drn-gn);
		exp=tmp1; 
		if(tmp2>tmp1)
		{
			exp=tmp2;
		}
		abcd = karat(gm-drm,gn-drn,exp); 

	}
	else if((gm-drm>=0)&&(gn-drn>=0)){
		long long int tmp1 = lead_exp(gm-drm);
		long long int tmp2 = lead_exp(gn-drn);
		exp=tmp1; 
		if(tmp2>tmp1)
		{
			exp=tmp2;
		}
		 
		abcd = karat(gm-drm,gn-drn,exp);
	}
	else if((gm-drm>=0)&&(gn-drn<0)){
		long long int tmp1 = lead_exp(gm-drm);
		long long int tmp2 = lead_exp(drn-gn);
		exp=tmp1; 
		if(tmp2>tmp1)
		{
			exp=tmp2;
		}
		abcd = -karat(gm-drm,gn-drn,exp); 
	}

	else if((gm-drm<0)&&(gn-drn>=0)){
		long long int tmp1 = lead_exp(drm-gm);
		long long int tmp2 = lead_exp(gn-drn);
		exp=tmp1; 
		if(tmp2>tmp1)
		{
			exp=tmp2;
		}
		abcd = -karat(gm-drm,gn-drn,exp); 
	}
	
	
	
	return ( ac<<(exp_moit<<1) )+(( ac + bd - abcd )<<exp_moit )+ bd;
	
}
//*/

int main(int argc, char ** argv )
{	
	/*
	for(long long int i=0;i<100;i++)
	{
;		printf("lead_exp(%lld)= %lld",i,lead_exp(i));
		printf("\n");
		printf("En binaire %lld s'écrit %lld",i,binaire(i));
		printf("\n");
	}
	//*/

	//*

	long long int m= atoi(argv[1]);
	long long int n= atoi(argv[2]);
	long long int mexp= lead_exp(m);
	long long int nexp= lead_exp(n);
/*
	printf("En binaire %lld s'écrit %lld",m,binaire(m));
	printf("\n");
	printf("En binaire %lld s'écrit %lld",n,binaire(n));
	printf("\n");
	printf("lead_exp m :%lld ",mexp);
	printf("\n");
	printf("lead_exp n :%lld ",nexp);
	printf("\n");
//*/



	if(mexp!=nexp){
		printf("les deux nombres ne sont pas de même taille en base 2");
		return -1;
	}
	long long int exp = mexp;

	long long int r = karat(m,n, exp);
	printf("%lld",r);
	printf("\n");
	//*
	float c=0;
	float c2=0;
	for(long long int i = 0; i<100;i++){
		for(long long int j = i; j<100;j++){
			exp = lead_exp(j);
			if(exp==lead_exp(i)){
				r= karat(i,j,exp);
				printf("karatsuba(%lld, %lld)=%lld",i,j,r);
				printf("\n");
				printf("%lld*%lld=%lld",i,j,i*j);
				printf("\n");
				c2++;
				if(r==i*j)
				{
					c++;
				}
			}
		}	
	}
	c=c/c2;
	printf("ratio de succès:%f %%",c);
	printf("\n");
	//*/
	return 0;
}