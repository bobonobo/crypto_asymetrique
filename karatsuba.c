#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

long long int lead_exp(long long int m)
{
	if(m==0)
	{
		return 0;
	}

	long long int tmp=m;
	long long int i =0;
	while(tmp!=0)
	{
		tmp=tmp>>1;
		i++;
	}
	i--;
	return i;
}


long long int binaire(long long int m)
{
	if(m<0){
		return -1;
	}
	long long int tmp=m;
	long long int exp = lead_exp(m);
	//exp--;
	long long int r = 0;
	while(exp!=-1)
	{
		if (( (tmp>>(exp))&1)==1)
		{
			r=r+1;
		}
		exp--;
		r=(r<<3)+(r<<1);
		
	}

	return r/10;
}

long long int karat(long long int m, long long int n)
{	
	if(n<0){
		n=-n;		
	}

	if(m<0){
		m=-m;		
	}

	if((n==0)||(m==0)){
		return 0;
	}
	long long int mexp= lead_exp(m);
	long long int nexp= lead_exp(n);
	long long int exp = mexp;
	
	if(nexp>mexp)
	{
		exp=nexp;
	}
	long long int exp_moit = (exp>>1)+1;
	/*
	long long int mask_g = (1<<(exp))-(1<<(exp_moit));
	if(mask_g==0)
	{
		mask_g=1;
	}
	printf("mask_g:%lld",binaire(mask_g));
	printf("\n");
	long long int mask_d = (1<<(exp_moit))-1;
	if(mask_d==0)
	{
		mask_d=1;
	}
	printf("mask_d:%lld",binaire(mask_d));
	printf("\n");
	*/
	if(exp==0){
		if(n==1 && m==1)
		{
			return 1;
		}
		return 0 ;
	}
	
	if(exp==1)
	{
		
		exp_moit=1;
	
		long long int gm = m>>exp_moit;
		long long int gn = n>>exp_moit;
		long long int drm = m-(gm<<exp_moit);
		long long int drn = n-(gn<<exp_moit);	
		
		long long int ac;
		if(gm==1 && gn==1)
		{
			ac= 1;
		}
		else
		{
			ac=0;
		} 
		long long int bd;
		if(drm==1 && drn==1)
		{
			bd= 1;
		}
		else
		{
			bd=0;
		} 
		long long int abcd=0; 
		
		if(((gm-drm)==1) && ((gn-drn)==1))
		{
			abcd= 1;
		}
		else if(((gm-drm)==1) && ((gn-drn)==-1))
		{
			abcd= -1;
		}
		else if(((gm-drm)==-1) && ((gn-drn)==1))
		{
			abcd= -1;
		}
		
		long long int r = ( ac<<2 )+(( ac + bd - abcd )<<1 )+ bd;
		return r ;

	}
	
	//*
	long long int gm = m>>exp_moit;
	long long int gn = n>>exp_moit;
	long long int drm = m-(gm<<exp_moit);
	long long int drn = n-(gn<<exp_moit);
	//*/
/*
	long long int gm = m&mask_g;
	long long int gn = n&mask_g;
	long long int drm = m&mask_d;
	long long int drn = n&mask_d;
//*/

/*
	printf("En binaire %lld s'écrit %lld",gm,binaire(gm));
	printf("\n");

	printf("En binaire %lld s'écrit %lld",gn,binaire(gn));
	printf("\n");


	printf("En binaire %lld s'écrit %lld",drm,binaire(drm));
	printf("\n");


	printf("En binaire %lld s'écrit %lld",drn,binaire(drn));
	printf("\n");
	
	printf("exp %lld ",exp);
	printf("\n");

	printf("exp_moit %lld ",exp_moit);
	printf("\n");
//*/		
	long long int ac = karat(gm,gn);
	long long int bd = karat(drm,drn);

	long long int abcd ;
	if((gm-drm<0)&&(gn-drn<0))
	{
		abcd = karat(gm-drm,gn-drn); 

	}
	else if((gm-drm>=0)&&(gn-drn>=0)){
		abcd = karat(gm-drm,gn-drn);
	}
	else if((gm-drm>=0)&&(gn-drn<0)){

		abcd = -karat(gm-drm,gn-drn); 
	}

	else if((gm-drm<0)&&(gn-drn>=0)){
		abcd = -karat(gm-drm,gn-drn); 
	}
	
	return ( ac<<(exp_moit<<1) )+(( ac + bd - abcd )<<exp_moit )+ bd;
	
}
//*/

int main(int argc, char ** argv )
{	
	/*
	for(long long int i=0;i<100;i++)
	{
;		printf("lead_exp(%lld)= %lld",i,lead_exp(i));
		printf("\n");
		printf("En binaire %lld s'écrit %lld",i,binaire(i));
		printf("\n");
	}
	//*/

	/*

	long long int m= atoi(argv[1]);
	long long int n= atoi(argv[2]);
	long long int mexp= lead_exp(m);
	long long int nexp= lead_exp(n);
	printf("En binaire %lld s'écrit %lld",m,binaire(m));
	printf("\n");
	printf("En binaire %lld s'écrit %lld",n,binaire(n));
	printf("\n");
	printf("lead_exp m :%lld ",mexp);
	printf("\n");
	printf("lead_exp n :%lld ",nexp);
	printf("\n");




	if(mexp!=nexp){
		printf("les deux nombres ne sont pas de même taille en base 2");
		return -1;
	}


	long long int r = karat(m,n);
	printf("%lld",r);
	printf("\n");
	//*/
	long long int m= atoi(argv[1]);
	long long int n= atoi(argv[2]);
	
	long long int r;
	float c=0;
	float c2=0;
	//long long int exp;
	for(long long int i = m-10; i<=m;i++){
		for(long long int j = n-10; j<=n;j++){
			//exp = lead_exp(j);
			//if(exp==lead_exp(i)){
			clock_t debut=clock();
			r= karat(i,j);
			clock_t difference = clock()-debut;
			printf("difference karat:%f ",(double)difference/CLOCKS_PER_SEC);
			printf("\n");
			debut=clock();
			i*j;
			difference = clock()-debut;
			printf("difference %lld*%lld ordi:%f",i,j,(double)difference/CLOCKS_PER_SEC);
			printf("\n");
			printf("karatsuba(%lld, %lld)=%lld",i,j,r);	
			printf("\n");
			printf("%lld*%lld=%lld",i,j,i*j);
			printf("\n");
			c2++;
			if(r==i*j)
			{
				c++;
			}
			//}
		}	
	}
	c=100*c/c2;
	printf("ratio de succès:%f %%",c);
	printf("\n");


	return 0;
}