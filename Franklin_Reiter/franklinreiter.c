#include <stdio.h>
#include<stdlib.h>

int inverse(int a, int n)
{
    int t = 0;
    int newt = 1;
    int r = n;
    int newr = a;
    int q;
    int tempt;
    int tempr;
    while (newr!=0)
    {
        q = r/newr;
        tempt=newt;
        newt= t-q*newt;
        t=tempt;
        tempr=newr;
        newr= r-q*newr;
        r=tempt;
    } 
      
    if(r > 1)
    {
        printf("%d n'est pas inversible",a);
        return a;
    }
    if (t < 0){
        t= t + n;
    }
    return t;
}



struct Term
{
    int coeff;
    int exp;
};

struct Poly
{
    int n;
    struct Term *terms;
};

void create (struct Poly *p, int mod)
{
    int i;
    printf ("Enter Number of terms: ");
    scanf ("%d", &p->n);
    p->terms = (struct Term *) malloc (p->n * sizeof (struct Term));
    printf ("Enter terms:\n");
    for (i = 0; i < p->n; i++)
    {
        scanf ("%d%d", &p->terms[i].coeff, &p->terms[i].exp);
        p->terms[i].coeff=(p->terms[i].coeff)%mod;
    }
    printf ("\n");
}
void display (struct Poly p)
{
    int i;
    for (i = 0; i < p.n; i++)
    {
        printf ("%dx%d", p.terms[i].coeff, p.terms[i].exp);
        if (i + 1 < p.n)
         printf (" + ");
    }
    printf ("\n");
}
struct Poly *add (struct Poly *p1, struct Poly *p2, int mod)
{
    int i, j, k;
    struct Poly *sum;
    sum = (struct Poly *) malloc (sizeof (struct Poly));
    sum->terms = (struct Term *) malloc ((p1->n + p2->n) * sizeof (struct Term));
    i = j = k = 0;
    while (i < p1->n && j < p2->n)
    {
        if (p1->terms[i].exp > p2->terms[j].exp)
        {
            sum->terms[k++] = p1->terms[i++];
        }
        else if (p1->terms[i].exp < p2->terms[j].exp)
        {
            sum->terms[k++] = p2->terms[j++];
        }
        else
        {
            sum->terms[k].exp = p1->terms[i].exp;

            if((p1->terms[i++].coeff + p2->terms[j++].coeff)%mod==0)
            {
                t++;
                sub->terms[k++].coeff =0;
            }
            else
            {
                sub->terms[k++].coeff = (p1->terms[i++].coeff + p2->terms[j++].coeff)%mod;    
            }
          
        }
    }
    for (; i < p1->n; i++)
    {
        sum->terms[k++] = p1->terms[i];
    }
    for (; j < p2->n; j++)
    {
        sum->terms[k++] = p2->terms[j];
    }
    sum->n = k;
    return sum;
}

struct Poly *sub (struct Poly *p1, struct Poly *p2, int mod)
{
    int i, j, k;
    int t = 0;
    struct Poly *sub;
    sub = (struct Poly *) malloc (sizeof (struct Poly));
    sub->terms = (struct Term *)malloc ((p1->n + p2->n) * sizeof (struct Term));
    i = j = k = 0;
    while (i < p1->n && j < p2->n)
    {
        if (p1->terms[i].exp > p2->terms[j].exp)
        {
            sub->terms[k++] = p1->terms[i++];
        }
        else if (p1->terms[i].exp < p2->terms[j].exp)
        {
            sub->terms[k++] = p2->terms[j++];
        }
        else
        {
            sub->terms[k].exp = p1->terms[i].exp;

            if((p1->terms[i++].coeff - p2->terms[j++].coeff)%mod==0)
            {
                t++;
                sub->terms[k++].coeff =0;
            }
            else if(p1->terms[i++].coeff - p2->terms[j++].coeff>0)
            {
                sub->terms[k++].coeff = (p1->terms[i++].coeff - p2->terms[j++].coeff)%mod;    
            }
            else if (p1->terms[i++].coeff - p2->terms[j++].coeff<0)
            {
                int temp = p1->terms[i++].coeff - p2->terms[j++].coeff;
                while(temp<0)
                {
                    temp+=mod;
                }
                sub->terms[k++].coeff = temp%mod;
            }

        }
    }
    for (; i < p1->n; i++)
    {
        sub->terms[k++] = p1->terms[i];
    }
    for (; j < p2->n; j++)
    {
        sub->terms[k++] = p2->terms[j];
    }
    sub->n = k-t;
    return sub;
}

struct Poly *gcdpol(struct Poly *p1, struct Poly *p2)
{

}




int main()
{
    struct Poly p1, p2, *p3;
    printf ("Enter Polynomial 1:\n");
    create (&p1);
    printf ("Enter Polynomial 2:\n");
    create (&p2);
    p3 = add (&p1, &p2);
    printf ("\n");
    printf ("Polynomial 1 is: ");
    display (p1);
    printf ("\n");
    printf ("Polynomial 2 is: ");
    display (p2);
    printf ("\n");
    printf ("Polynomial 3 is: ");
    display (*p3);
    return 0;
}