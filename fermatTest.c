#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions2.h"

int fermat(long long unsigned int n, int p){
	int k=0;
	while(k<p){
			srand(time(NULL));
			long long unsigned int b = rand()%(n-2)+1;
			if(pgcd(b,n)==1){
				if(expmod2(b,n-1,n)!=1){
					printf("n n'est pas premier");
					return -1;
				}
				else{
					k++;
				}
			}
			else{
				printf("n n'est pas premier");
				return -1;
			}
	}
	float d =(float)(1<<p);
	printf("d:%f \n",d);
	float prob =1-1/d;
	printf("selon fermat n est probablement premier avec une probabilité supérieure à %f", prob);
	return 0;
}

int solovay(long long unsigned int n, int p){
	int k=0;
	while(k<p){
			srand(time(NULL));
			long long unsigned int b = rand()%(n-2)+1;
			if(pgcd(b,n)==1){
				if(expmod2(b,((n-1)>>1),n)!=jacobi(b,n)){
					printf("n n'est pas premier");
					return -1;
				}
				else{
					k++;
				}
			}
			else{
				printf("n n'est pas premier");
				return -1;
			}
	}
	float d =(float)(1<<p);
	printf("d:%f \n",d);
	float prob =1-1/d;
	printf("selon Solovay Strassen n est probablement premier avec une probabilité supérieure à %f", prob);
	return 0;
}
int main(int argc, char **argv){
	long long unsigned int n = (long long unsigned int) atoi(argv[1]);
	int p = atoi(argv[2]);
	fermat(n,p);
	solovay(n,p);
	for(long long unsigned int i=15;i<100;i++){
		fermat(i,p);
	}
}