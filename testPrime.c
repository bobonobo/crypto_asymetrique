#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctions2.h"

int main(int argc, char **argv){
	if(argc!=3){
		printf("donnez 2 arguments: le nombre à tester, et la précision. exemple ./test 17 10\n");
		return -1;
	}
	long long unsigned int n = (long long unsigned int) atoi(argv[1]);
	int p = atoi(argv[2]);
	fermat(n,p);
	solovay(n,p);
	millerRabin(n,p);
	//*
	long long unsigned int i; 
	for(i=4;i<102;i++){
		fermat(i,p);
		solovay(i,p);
		millerRabin(i,p);
	}
	//*/
}