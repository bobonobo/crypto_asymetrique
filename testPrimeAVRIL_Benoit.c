#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include "fonctionsAVRIL_BENOIT.h"

int fermat(long long unsigned int n, int p){
	int k=0;
	srand(time(NULL));
	while(k<p){
			long long unsigned int b = rand()%(n-2)+1;
			if((n&1)==0){
				printf("%lld composé (Fermat)\n",n);
				return 0;
			}
			if(pgcd(b,n)==1){
				if(expmod3(b,n-1,n)!=1){
					printf("%lld composé (Fermat)\n",n);
					return 0;
				}
				else{
					k++;
				}
			}
			else{
				printf("%lld composé (Fermat)\n",n);
				return 0;
			}
	}
	float d =(float)(1<<p);
	//printf("d:%f \n",d);
	float prob =1-1/d;
	printf("%lld probablement premier, p>= %f (Fermat)\n",n,prob);

	return 1;
}

int solovay(long long unsigned int n, int p){
	int k=0;
	srand(time(NULL));
	while(k<p){

			long long unsigned int b = rand()%(n-3)+2;
			if((n&1)==0){
				printf("%lld composé (Solovay Strassen)\n",n);
				return 0;
			}
			if(pgcd(b,n)==1){
				long long unsigned int e = expmod3(b,((n-1)>>1),n);
				long long int j =jacobi((long long int) b,(long long int)n);
				j=(j<0)?j+n:j;
				if((long long int)e!=j){
					printf("%lld composé (Solovay Strassen)\n",n);
					return 0;
				}
				else{
					k++;
				}
			}
			else{
				printf("%lld composé (Solovay Strassen)\n",n);
				return 0;
			}
	}
	float d =(float)(1<<p);
	float prob =1-1/d;
	printf("%lld probablement premier, p>= %f (Solovay Strassen)\n",n,prob);
	return 1;
}
int isWitness(long long unsigned int a,long long unsigned int d,long long unsigned int n, int s){
	long long unsigned int e=expmod3(a,d,n);
	if(e==1||e==n-1){
		return 0;
	}
	else{
		for(int i=0;i<s-1;i++){
			e=sqrmod1(e,n);
			if(e==n-1){
				return 0;			
			}
		}
	}
	return 1;
}

int millerRabin(long long unsigned int n, int p){
	if((n&1)==0){
		printf("%lld composé (Miller Rabin)\n",n);
		return 0;
	}
	int s=0;
	long long unsigned int d=1;
	long long unsigned int temp = n-1;
	while((temp&1)==0){
		temp>>=1;
		s++;
	}
	d=((n-1)>>s);
	int k=0;
	srand(time(NULL));
	while(k<p){
			long long unsigned int a = rand()%(n-2)+2;
			if(isWitness(a,d,n,s)==1){
				printf("%lld composé (Miller Rabin)\n",n);
				return 0;
			}
			k++;
	}
	double pui =(double)pow(0.25,(double)p);
	//printf("d:%f \n",d);
	double prob =1-pui;
	printf("%lld probablement premier, p>= %f (Miller Rabin)\n",n,prob);
	return 1;
}

int main(int argc, char **argv){
	if(argc!=3){
		printf("donnez 2 arguments: le nombre à tester, et la précision. exemple ./test 17 10\n");
		return -1;
	}
	long long unsigned int n = (long long unsigned int) atoi(argv[1]);
	int p = atoi(argv[2]);
	fermat(n,p);
	solovay(n,p);
	millerRabin(n,p);
	//*
	long long unsigned int i; 
	for(i=4;i<102;i++){
		fermat(i,p);
		solovay(i,p);
		millerRabin(i,p);
	}
	//*/
}